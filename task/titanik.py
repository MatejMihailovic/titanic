import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    mr_subset = df[df['Name'].str.contains('Mr.')]
    mr_subset_na_num = mr_subset['Age'].isna().sum()
    mr_subset_mean_value = round(mr_subset['Age'].mean())
    mr_subset_copy = mr_subset.copy()
    mr_subset_copy['Age'].fillna(value=mr_subset_mean_value, inplace=True)
    df[df['Name'].str.contains('Mr.')] = mr_subset_copy

    mrs_subset = df[df['Name'].str.contains('Mrs.')]
    mrs_subset_na_num = mrs_subset['Age'].isna().sum()
    mrs_subset_mean_value = round(mrs_subset['Age'].mean())
    mrs_subset_copy = mrs_subset.copy()
    mrs_subset_copy['Age'].fillna(value=mrs_subset_mean_value, inplace=True)
    df[df['Name'].str.contains('Mrs.')] = mrs_subset_copy

    miss_subset = df[df['Name'].str.contains('Miss.')]
    miss_subset_na_num = miss_subset['Age'].isna().sum()
    miss_subset_mean_value = round(miss_subset['Age'].mean())
    miss_subset_copy = miss_subset.copy()
    miss_subset_copy['Age'].fillna(value=miss_subset_mean_value, inplace=True)
    df[df['Name'].str.contains('Miss.')] = miss_subset_copy

    return [('Mr.', mr_subset_na_num, mr_subset_mean_value), ('Mrs.', mrs_subset_na_num, mrs_subset_mean_value), ('Miss.', miss_subset_na_num, miss_subset_mean_value)]

if __name__ == "__main__":
    print(get_filled())
